package com.example.android.sensormanager;

import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private boolean color=false;
    private View view;
    private long lastUpdate;
    Button mBtnCompassAct,mBtnGotoDetails,mLightBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view=findViewById(R.id.textView);
        view.setBackgroundColor(Color.GREEN);

        sensorManager= (SensorManager) getSystemService(SENSOR_SERVICE);

        lastUpdate=System.currentTimeMillis();
        mBtnCompassAct=findViewById(R.id.btnCompassActivity);
        mLightBtn=findViewById(R.id.btnLightActivity);
        mBtnCompassAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,CompassActivity.class));
            }
        });

        mBtnGotoDetails=findViewById(R.id.btnDetailActivity);
        mBtnGotoDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,SensorDetails.class));

            }
        });

        mLightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,LightSensorActivity.class));
            }
        });

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER);
        getAccelerometer(event);
    }
    private void getAccelerometer(SensorEvent event) {
        float[] values=event.values;

        float x=values[0];
        float y=values[1];
        float z=values[2];

        float acceletionSquareRoot=(x*x+y*y+z*z)/(SensorManager.GRAVITY_EARTH*SensorManager.GRAVITY_EARTH);
        long actualTime=event.timestamp;
        if(acceletionSquareRoot>=2){
            if(actualTime-lastUpdate<200){
                return;
            }
            lastUpdate=actualTime;
            Toast.makeText(this, "Device Suffeled", Toast.LENGTH_SHORT).show();
            if(color){
                view.setBackgroundColor(Color.GREEN);
            }
            else{
                view.setBackgroundColor(Color.RED);
            }
            color=!color;
            Log.e("NEW x axis value", String.valueOf(x));
            Log.e("NEW y axis value", String.valueOf(y));
            Log.e("NEW z axis value", String.valueOf(z));
            Log.e("Gravity Earth value", String.valueOf(SensorManager.GRAVITY_EARTH));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor
                .TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }
}
